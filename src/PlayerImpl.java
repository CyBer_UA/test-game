import game.Game;
import game.Player;
import game.Position;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public class PlayerImpl implements Player {

    @Override public void update(final Game game) {
        Position positionCurrent = game.getPositionCurrent(); // получаем позицию игрока :)
        int[][] map = game.getMap();//получаем игровую карту, карта это массив из 0 и 1, где 1 это стена.
        // победа наступит тогда когда игрок дойдет позиции x: 0, y: 9, так же стоит учесть что дойти до этой позиции
        // может быть не возможно)
        game.moveBottom();//перемещаем вниз, если игрок выйдет за границу или пытается пройти через стену будет исключение
    }
}
