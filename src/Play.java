import game.Game;
import game.GameImpl;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public class Play {
    public static void main(String[] args) {
        Game game = new GameImpl(new PlayerImpl());
    }

}
