package game;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public class Position {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Position(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    @Override public boolean equals(final Object obj) {
        return  ((Position)obj).getX() == x && ((Position)obj).getY() == y;
    }
}
