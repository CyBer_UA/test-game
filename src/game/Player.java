package game;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public interface Player {
    void update(Game game);
}
