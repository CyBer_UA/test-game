package game;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public interface Game {
    void moveRight();

    void moveTop();

    void moveLeft();

    void moveBottom();

    int[][] getMap();

//    boolean isWall(Position position);

    Position getPositionCurrent();
}
