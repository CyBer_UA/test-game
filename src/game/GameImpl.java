package game;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Copyright (c) Asseco Business Solutions S.A. All rights reserved.
 */
public class GameImpl implements Game {

    private final Player player;

    private int[][] map;

    private Timer timer = new Timer();

    private Position position = new Position(0, 0);

    public GameImpl(Player player) {
        this.player = player;
        map = generateMap(10, 10);
        printMap();
        run();
    }

    private void run() {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                player.update(GameImpl.this);
                validate();
                printMap();
                checkWin();

            }
        }, 0, 1000);
    }

    @Override public void moveRight() {
        position = new Position(position.getX() + 1, position.getY());
    }

    @Override public void moveTop() {
        position = new Position(position.getX(), position.getY() - 1);
    }

    @Override public void moveLeft() {
        position = new Position(position.getX() - 1, position.getY());
    }

    @Override public void moveBottom() {
        position = new Position(position.getX(), position.getY() + 1);
    }
    private void validate() {
        if(position.getY() < 0 ||
            position.getY() < 0 ||
            position.getY() >= 10 ||
            position.getX() >= 10 ||
            map[position.getY()][position.getX()] == 1) {
            throw new Error("incorrect position");
        }
    }

    private void checkWin() {
        if(position.equals(new Position(0, 9))) {
            timer.cancel();
            timer.purge();
            System.out.println("You win!!");
        }
    }

    @Override
    public int[][] getMap() {
        return map;
    }

    @Override public Position getPositionCurrent() {
        return position;
    }

    private void printMap() {
        System.out.println("");
        System.out.println("----------------");
        System.out.println("");
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (new Position(j, i).equals(position)) {
                    System.out.print("L");
                } else {
                    printItem(map[i][j]);
                }
            }
            System.out.println("");
        }
    }

    private void printItem(int item) {
        if (item == 1) {
            System.out.print("#");
        } else if (item == 0) {
            System.out.print("_");
        }
    }

    private int[][] generateMap(int sizeX, int sizeY) {
        int[][] map = new int[sizeX][sizeY];

        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                Random rand = new Random();
                map[i][j] = rand.nextInt(2);
            }
        }

        map[0][0] = 0;
        map[9][0] = 0;
        map[9][1] = 0;
        map[8][0] = 0;
        return map;
    }

}
